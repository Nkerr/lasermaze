/* Created by Nathaniel Kerr
 * 11/2/2018
 * lasermaze program for Sizmek
 * 
 * logicMaster is responsible for dividing the string from the test case file into a discernible matrix
 */

package lasermazepackage;

public class logicMaster {
	char[][] symbols = new char[100][100];
	String[] divider = new String[900];
	int row = 0, col = 0, rows;
	
	//receives the file string and the number of rows included
	public void inputStrings(String maze, int rowcount) {
		
		cellType lasermaze = new cellType();
		String split = "\r\n";
		rows = rowcount;
		divider = maze.split(split, rows);
		
		/*
		 * file string is split by every line break. Each symbol is respectively placed in it's own column. 
		 * When the line ends a new row is created and the process repeats
		 * 
		 */
		try {
			for(row=0; row<(divider.length - 1); row++) {

				for(col=0; col<divider[row].length(); col++) {
					symbols[row][col] = divider[row].charAt(col);

				}
			}
		}
		catch(ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		//cell sorting object receives the matrix via three variables, the characters in the file string, the number of rows and columns.
		lasermaze.inputMaze(symbols, row, col);
	}
}
