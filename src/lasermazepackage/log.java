/* Created by Nathaniel Kerr
 * 11/2/2018
 * lasermaze program for Sizmek
 */

package lasermazepackage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class log {
	
	//receives the number of movements made by the laser in the maze
	public void input(int laserCounter) {
		
        File pathname = new File(System.getProperty("user.dir")+"\\test.txt");  
        BufferedWriter buffWrite;
        
        //if the file doesn't already exist, creates a new file
        if(!pathname.exists())
        {
            try 
            {
                pathname.createNewFile();
                pathname.setWritable(true);
            }
            catch (IOException e) 
            {
                    System.out.println("Failure to create log file");
            }
        }
        //Attempts to write the number of laser movements to the file, and then closes the writer.
        try 
        {
        	
        	buffWrite = new BufferedWriter(new FileWriter(pathname, true));
        	buffWrite.write(Integer.toString(laserCounter));
            buffWrite.close();
        }
        
        catch (IOException e)
        {
        	System.out.println("Failure to initiate buffered writer for log");
        }
        
    }
}
