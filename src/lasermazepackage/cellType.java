/* Created by Nathaniel Kerr
 * 11/2/2018
 * lasermaze program for Sizmek
 */

package lasermazepackage;


public class cellType {
	String name;
	boolean end = false;
	boolean start = false;
	int location, laserCounter;
	String dir = "right";
	
	//variables for current x,y position and previous x,y position
	int x , y, px, py;
	
	log logger = new log();
	loopObject[] checker = new loopObject[300];
	
	//method receives the matrix and begins laser travel. This executes the natural direction of the current laser path.
	public void inputMaze(char[][] maze, int length, int width) {

		//loop traveling through the maze to find the starting position of the laser.
		for(x=0; x<(length-1); x++) {
			y=0;
			while(y<(width-1)) {
				if(maze[y][x] == '@') {
					start = true;
					break;
				}
				y++;
			}
			if(start == true)
				break;
		}
		
		//Loop performs while an infinite loop is not detected and a wall in the maze has not been reached. 
		while(loop(x,y,dir) && end == false) {
			
			//switch case control flow to move the laser through the maze according to it's currently assigned direction.
			switch(dir) {
			case("up"):
				if(y==0){
					end = true;
					break;
				}
				y--;
				break;
				
			case("down"):
				if(y==length){
					end = true;
					break;
				}
				y++;
				break;
				
			case("left"):
				if(x==0){
					end = true;
					break;
				}
				x--;
				break;
				
			case("right"):
				if(x==width) {
					end = true;
					break;
				}
				x++;
				break;
				
			}
			//conditional statement finding the maze wall to end the flow
			if(maze[y][x] == ' ') {
				end = true;
			}
			
			//conditional statement to continue if the end has not been reached
			if(end == false) {
				laserCounter++;
				inputChar(maze[y][x]);
			}
			
			
		}
		//when the loop has ended (the laser has reached the wall) sneds the number of movements to the logger object
		logger.input(laserCounter);
		
	}
	
	//method to change laser direction upon reaching a specific character. A new case may be created to define different laser direction.
	public void inputChar(char laserPos) {
		//laser redirection method
		
		//switch/case control flow utilizing previous laser position and current laser position to determine where it should go next.
		switch(laserPos) {
		case '/':
			if(px > x) {
				px = x;
				py = y;
				dir = "down";
			}
			else if(px < x) {
				px = x;
				py = y;
				dir = "up";
			}
			else if(py < y) {
				px = x;
				py = y;
				dir = "left";
				
			}
			else if(py > y) {
				px = x;
				py = y;
				dir = "right";
			}
			break;
			
		case '\\':
			if(px < x) {
				px = x;
				py = y;
				dir = "down";
			}
			else if(px > x) {
				px = x;
				py = y;
				dir = "up";
			}
			else if(py > y) {
				px = x;
				py = y;
				dir = "left";
				
			}
			else if(py < y) {
				px = x;
				py = y;
				dir = "right";
			}
			break;
		
		case '0':
		case 'O':
			if(px > x) {
				px = x;
				py = y;
				dir = "right";
			}
			if(px < x) {
				px = x;
				py = y;
				dir = "left";
			}
			if(py > y) {
				px = x;
				py = y;
				dir = "down";
			}
			if(py > y) {
				px = x;
				py = y;
				dir = "up";
			}
			break;
			
		case 'V':
		case 'v':
			px = x;
			py = y;
			dir = "down";
			break;
			
		case '^':
			px = x;
			py = y;
			dir = "up";
			break;
			
		case '>':
			px = x;
			py = y;
			dir = "right";
			break;
			
		case '<':
			px = x;
			py = y;
			dir = "left";
			break;
			
		case '@':
			px = x;
			py = y;
			start = true;
			break;
		case '-':
			px = x;
			py = y;
			break;
			
		}
		//System.out.println("detected");	
		

	}
	//Contingency method; runs to discover an infinite loop.
	public boolean loop(int xTrack, int yTrack, String symbol) {
		//method detects pattern based on laser traveling the same path
		
		int counter = 0;
		//using the infinite loop object that sets a key for each laser position in the maze
		checker[laserCounter] = new loopObject(xTrack, yTrack, symbol);
		
		/*
		 * Loop runs through each position the laser was previously in to determine if it was going in the same direction across the same point previously.
		 * Matches the current created key against prior keys formed from laser position and direction.
		 */
		while(counter < laserCounter && end == false) {
			if(checker[counter].key.equals(checker[laserCounter].key)) {
				laserCounter = -1;
				//sets the counter to -1 and returns false if infinite loop detected
				return false;
			}
			counter++;
		}
		//upon reaching this point, no infinite loop has been detected, laser may continue
		return true;		
	}
	
}
