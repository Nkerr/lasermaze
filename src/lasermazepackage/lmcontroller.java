/* Created by Nathaniel Kerr
 * 11/2/2018
 * lasermaze program for Sizmek
 * lmcontroller is the central controller for the laser maze program
 */

package lasermazepackage;

public class lmcontroller {
	
	public static void main(String[] args) {
		
		String mazeString = null;
		inputReader inputR = new inputReader();
		logicMaster laserLogic = new logicMaster();
		
		//Conditional statement to ensure test case is presented. Args.length represents how many arguments were provided for program launch.
		if(args.length < 1) {
			System.out.println("please present test case file");
			System.exit(1);
		}
		
		//Loop to assess each argument provided
		for(int i=0;i<args.length;i++) {
			
			//inputR object receives test case file name for inputFile method
			inputR.inputFile(args[i]);
			
			//inputR object method running until it reaches the end of it's file
			if(inputR.readFile() != null) {
				mazeString = inputR.readFile();
				break;
			}
		}
		
		//conditional statement informing user of a failure to read the file.
		if(mazeString.isEmpty()) {
			System.out.println("failure to read file");
			System.exit(1);
		}
		
		//using laserLogic object with the string from the file and the number of lines in the file
		laserLogic.inputStrings(mazeString, inputR.retCount());
		
	}
}
